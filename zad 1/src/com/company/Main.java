package com.company;

class EmptyDataException extends Exception {}
class Main {
    // rzuca wyjatek EmptyDataException gdy parametr wejsciowy jest pusty
    private double getAverage(final String digits)throws EmptyDataException {
        double sum = 0;
        String[] numbersArray = digits.split(",");

        for (int i = 0; i < numbersArray.length; i++){
            sum += Double.parseDouble(numbersArray[i]);
        }

        return sum/numbersArray.length;
    }

    public static void main(String[] args) {
        final String listOfDigits =
                "1.0,2.0,3.5,4.5,5.0,5.5,6.0,10.1,5.2,4.3,2.1,1.1";
        final Main main = new Main();

        try {
            double average = main.getAverage(listOfDigits);
            System.out.printf("avg(%s) = %.2f \n", listOfDigits, average);
        } catch(EmptyDataException e){
            System.out.println("Lista nie moze byc pusta!");
        }
    }
}