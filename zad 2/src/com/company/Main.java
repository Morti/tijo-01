package com.company;

interface RulesOfGame {
    // metoda sprawdza czy ruch jest poprawny dla figury.
    boolean isCorrectMove(Point2d moveFrom, Point2d moveTo);
}

class Point2d {
    private int x;
    private int y;

    public Point2d(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "{" +
                x +
                ", " + y +
                '}';
    }
}

// prosze zaimplementowac klase Rook, ktora implementuje interfejs RulesOfGame.

public class Main {

    public static void main(String[] args) {
        System.out.println("Test for ROOK");
        final RulesOfGame rook = new Rook();
        Point2d pointFrom = new Point2d(0, 0);
        Point2d pointTo = new Point2d(0, 10);
        System.out.printf("From %10s to %10s is %s \n",
                pointFrom, pointTo,
                rook.isCorrectMove(pointFrom, pointTo)); // true
        pointFrom = new Point2d(-10, -1);
        pointTo = new Point2d(10, -1);
        System.out.printf("From %10s to %10s is %s \n",
                pointFrom, pointTo,
                rook.isCorrectMove(pointFrom, pointTo)); // true
        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(5, 5);
        System.out.printf("From %10s to %10s is %s \n",
                pointFrom, pointTo,
                rook.isCorrectMove(pointFrom, pointTo)); // false
        pointFrom = new Point2d(1, 1);
        pointTo = new Point2d(-1, -2);
        System.out.printf("From %10s to %10s is %s \n",
                pointFrom, pointTo,
                rook.isCorrectMove(pointFrom, pointTo)); // false
    }
}

class Rook implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {
        if(moveFrom.getX() == moveTo.getX() || moveFrom.getY() == moveTo.getY()){
            return true;
        }else{
            return  false;
        }
    }
}